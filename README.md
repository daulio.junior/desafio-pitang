# Desafio - Pitang/TCE

Desafio técnico Java Pitang

# Sobre o desafio

O desafio consiste em criar aplicação que exponha uma API RESTful de criação de usuários e carros com login.

# Ferramentas utilizadas

1. Spring boot 2.7.2
2. Banco de dados H2
3. Java versão 11
4. Swagger
5. Testes unitários usando MockMvc

# Frontend

1. Angular 9.1.13
2. NodeJs 16.0.0
3. Angular Material 9.2.4

# Build Frontend

npm start

# Histórias de usuário

1. Gestão de Usuários:

* DJP1 - Como um usuário não autenticado, eu quero me cadastrar no sistema.

* DJP2 - Como um usuário não autenticado, eu quero ser capaz de ver uma lista de usuários para que eu possa ter um maior controle sobre os usuários cadastrados.

* DJP3 - Como um usuário não autenticado, eu quero ser capaz de atualizar informações dos usuários para corrigir erros ou atualizar detalhes.

* DJP4 - Como um usuário não autenticado, eu quero ser capaz de remover informações de usuários que não são mais relevantes.

* DJP5 - Como um usuário registrado e não autenticado, eu quero poder fazer login no sistema usando meu nome de usuário e senha.

* DJP6 - Como um usuário não autenticado, eu quero ser capaz de pesquisar informações de um usuário para que eu possa ter um maior controle sobre este usuário cadastrado.

* DJP7 - Como um usuário registrado e autenticado, eu quero cadastrar informações sobre meus carros para que eu possa acessá-las posteriormente.

* DJP8 - Como um usuário registrado e autenticado, eu quero ser capaz de ver uma lista de detalhes dos meus carros para que eu possa ter um maior controle sobre eles.

* DJP9 - Como um usuário registrado e autenticado, eu quero ser capaz de atualizar informações dos meus carros para corrigir erros ou atualizar detalhes.

* DJP10 - Como um usuário registrado e autenticado, eu quero ser capaz de remover informações de carros que não são mais relevantes.

* DJP11 - Como um usuário registrado e autenticado, eu quero poder visualizar minhas informações de login no sistema.

* DJP12 - Como um usuário registrado e autenticado, eu quero ser capaz de pesquisar informações de um carro para que eu possa ter um maior controle sobre ele.

# Solução

* **Spring Java:**

	Justificativa:
	
	O Spring Framework é uma plataforma de desenvolvimento de software Java robusta, flexível e escalável. Ele oferece diversos benefícios para o desenvolvimento de aplicações Java.
	
	Defesa Técnica:
	
	O uso do Spring Framework pode trazer diversos benefícios específicos, como redução do tempo de desenvolvimento, aumento da qualidade do código, melhora da manutenabilidade e redução de custos.

* **H2 Database:**	

	Justificativa:
	
	O H2 é um banco de dados relacional em memória de código aberto, leve e rápido. Ele é uma ótima opção para desenvolvimento e testes, pois é muito fácil de configurar e usar.
	
	Defesa Técnica:
	
	O H2 Database é fácil de configurar e usar, permitindo aos desenvolvedores testar e validar seus aplicativos de forma eficiente antes de escaloná-los para bancos de dados mais robustos em produção.

	
* **Swagger:**

  	Justificativa:

	O Swagger é uma ferramenta poderosa para documentação de API, permitindo que os desenvolvedores visualizem, interajam e testem chamadas de API diretamente pela interface Swagger.

	Defesa Técnica:
	
	A documentação clara e interativa da API é fundamental para facilitar a integração de serviços e para que outros desenvolvedores entendam como interagir com o sistema.
	O Swagger também simplifica a validação e os testes das APIs, melhorando a eficiência do processo de desenvolvimento e garantindo a qualidade do código.

* **Testes unitários:**

	Justificativa:
	
	Os testes unitários são uma prática essencial para garantir a qualidade do software. Eles servem para verificar se cada unidade de código (como uma função, método ou classe) está funcionando de acordo com o esperado.

	Defesa Técnica:
	
	Os testes unitários são uma prática essencial para garantir a qualidade do software. Eles podem ajudar a detectar erros, prevenir regressões, melhorar a qualidade do código e aumentar a confiança no código.


* **Angular 9.1.13:** 

	Justificativa:

	O Angular é um framework de desenvolvimento front-end amplamente utilizado para criar aplicativos da web escaláveis e robustos.

	A versão 9.1.13 do Angular oferece suporte a recursos avançados, como otimização de tamanho, melhorias de desempenho e correções de bugs.

	A comunidade Angular fornece suporte ativo e atualizações regulares para essa versão.

	Defesa Técnica:

	O Angular oferece uma arquitetura modular baseada em componentes, facilitando a criação de interfaces de usuário reutilizáveis.

	O uso de diretivas, serviços e injeção de dependência no Angular permite uma separação clara de responsabilidades.

	A CLI do Angular simplifica a criação, compilação e implantação de aplicativos.


* **Node.js 16.0.0:** 

	Justificativa:

	O Node.js é um ambiente de tempo de execução JavaScript que permite a execução de código JavaScript no servidor.

	A versão 16.0.0 traz melhorias de desempenho, correções de segurança e novos recursos.

	A comunidade Node.js oferece suporte ativo e atualizações frequentes.

	Defesa Técnica:

	O Node.js permite a criação de aplicativos escaláveis e assíncronos.

	O gerenciador de pacotes npm (incluído no Node.js) facilita a instalação e gerenciamento de bibliotecas e módulos.


* **Angular Material 9.2.4:** 

	Justificativa:

	O Angular Material é uma biblioteca de componentes de interface do usuário baseada no Material Design.

	A versão 9.2.4 oferece componentes prontos para uso, estilos consistentes e acessibilidade.

	Defesa Técnica:

	O Angular Material fornece componentes como botões, barras de navegação, tabelas, caixas de diálogo e muito mais.

	A padronização visual do Material Design ajuda a criar interfaces coesas e agradáveis.

	A manutenção e atualização do Angular Material são simplificadas com a CLI do Angular.