# Desafio - Pitang/TCE

Desafio técnico Java Pitang

# Sobre o desafio

O desafio consiste em criar aplicação que exponha uma API RESTful de criação de usuários e carros com login.

# Ferramentas utilizadas

1. Angular 9.1.13
2. NodeJs 16.0.0
3. Angular Material 9.2.4

# Build 

npm start

# Histórias de usuário

1. Gestão de Usuários:

* DJP1 - Como um usuário não autenticado, eu quero me cadastrar no sistema.

* DJP2 - Como um usuário não autenticado, eu quero ser capaz de ver uma lista de usuários para que eu possa ter um maior controle sobre os usuários cadastrados.

* DJP3 - Como um usuário não autenticado, eu quero ser capaz de atualizar informações dos usuários para corrigir erros ou atualizar detalhes.

* DJP4 - Como um usuário não autenticado, eu quero ser capaz de remover informações de usuários que não são mais relevantes.

* DJP5 - Como um usuário registrado e não autenticado, eu quero poder fazer login no sistema usando meu nome de usuário e senha.

* DJP6 - Como um usuário não autenticado, eu quero ser capaz de pesquisar informações de um usuário para que eu possa ter um maior controle sobre este usuário cadastrado.

* DJP7 - Como um usuário registrado e autenticado, eu quero cadastrar informações sobre meus carros para que eu possa acessá-las posteriormente.

* DJP8 - Como um usuário registrado e autenticado, eu quero ser capaz de ver uma lista de detalhes dos meus carros para que eu possa ter um maior controle sobre eles.

* DJP9 - Como um usuário registrado e autenticado, eu quero ser capaz de atualizar informações dos meus carros para corrigir erros ou atualizar detalhes.

* DJP10 - Como um usuário registrado e autenticado, eu quero ser capaz de remover informações de carros que não são mais relevantes.

* DJP11 - Como um usuário registrado e autenticado, eu quero poder visualizar minhas informações de login no sistema.

* DJP12 - Como um usuário registrado e autenticado, eu quero ser capaz de pesquisar informações de um carro para que eu possa ter um maior controle sobre ele.


# Solução


* **Angular 9.1.13:** 

	Justificativa:

	O Angular é um framework de desenvolvimento front-end amplamente utilizado para criar aplicativos da web escaláveis e robustos.

	A versão 9.1.13 do Angular oferece suporte a recursos avançados, como otimização de tamanho, melhorias de desempenho e correções de bugs.

	A comunidade Angular fornece suporte ativo e atualizações regulares para essa versão.

	Defesa Técnica:

	O Angular oferece uma arquitetura modular baseada em componentes, facilitando a criação de interfaces de usuário reutilizáveis.

	O uso de diretivas, serviços e injeção de dependência no Angular permite uma separação clara de responsabilidades.

	A CLI do Angular simplifica a criação, compilação e implantação de aplicativos.


* **Node.js 16.0.0:** 

	Justificativa:

	O Node.js é um ambiente de tempo de execução JavaScript que permite a execução de código JavaScript no servidor.

	A versão 16.0.0 traz melhorias de desempenho, correções de segurança e novos recursos.

	A comunidade Node.js oferece suporte ativo e atualizações frequentes.

	Defesa Técnica:

	O Node.js permite a criação de aplicativos escaláveis e assíncronos.

	O gerenciador de pacotes npm (incluído no Node.js) facilita a instalação e gerenciamento de bibliotecas e módulos.


* **Angular Material 9.2.4:** 

	Justificativa:

	O Angular Material é uma biblioteca de componentes de interface do usuário baseada no Material Design.

	A versão 9.2.4 oferece componentes prontos para uso, estilos consistentes e acessibilidade.

	Defesa Técnica:

	O Angular Material fornece componentes como botões, barras de navegação, tabelas, caixas de diálogo e muito mais.

	A padronização visual do Material Design ajuda a criar interfaces coesas e agradáveis.

	A manutenção e atualização do Angular Material são simplificadas com a CLI do Angular.
