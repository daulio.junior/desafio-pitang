package com.daulio.desafiotcebackend.controller;


import com.daulio.desafiotcebackend.dto.UserDTO;
import com.daulio.desafiotcebackend.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Representa o controller de usuários no sistema.
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/users")
@Api(tags = "UserController - Representa o controller de usuários no sistema.")
public class UserController {

    private final UserService service;

    /**
     * Recupera a lista de todos os usuários
     *
     */
    @GetMapping()
    @ApiOperation(value = "Recupera a lista de todos os usuários")
    public ResponseEntity<?> getAll(){
        try{
            return ResponseEntity.ok(this.service.findAll());
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        }
    }

    /**
     * Salva um novo usuário
     *
     * @param dto objeto com os dados do novo usuário
     */
    @PostMapping()
    @ApiOperation(value = "Salva um novo usuário")
    public ResponseEntity<?> save(@RequestBody UserDTO dto) {
        try{
            return ResponseEntity.ok(this.service.create(dto));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        }
    }

    /**
     * Recupera um usuário por id
     *
     * @param id do usuário para perquisa
     */
    @ApiOperation(value = "Recupera um usuário por id")
    @GetMapping("{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(this.service.findById(id));
    }

    /**
     * Atualiza os dados usuário
     *
     * @param dto objeto com os dados atualizado do usuário
     */
    @PutMapping()
    @ApiOperation(value = "Atualiza os dados usuário")
    public ResponseEntity<?> update(@RequestBody UserDTO dto) {
        try{
            return ResponseEntity.ok(this.service.update(dto));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        }
    }

    /**
     * Exclui um usuário por id
     *
     * @param id do usuário a ser excluído
     */
    @DeleteMapping("{id}")
    @ApiOperation(value = "Exclui um usuário por id")
    public ResponseEntity<?> deleteById(@PathVariable Long id) {
        try{
            this.service.deleteById(id);
            return ResponseEntity.ok(Boolean.TRUE);
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        }

    }



}

