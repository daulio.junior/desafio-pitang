package com.daulio.desafiotcebackend.controller;

import com.daulio.desafiotcebackend.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Representa o controller que traz dados do usuário logado no sistema
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/me")
@Api(tags = "MeController - Representa o controller que traz dados do usuário logado no sistema")
public class MeController {

    private final UserService service;

    @GetMapping()
    @ApiOperation(value = "Recupera os dados do usuário logado")
    public ResponseEntity<?> findAuthenticateUser(@RequestHeader("Authorization") String token) {
        try {
          return ResponseEntity.ok(this.service.findAuthenticateUserByToken(token));
        }catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }
}
