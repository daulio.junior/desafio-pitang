package com.daulio.desafiotcebackend.controller;

import com.daulio.desafiotcebackend.dto.CarDTO;
import com.daulio.desafiotcebackend.service.CarService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Representa o controller de carros do usuário no sistema
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/cars")
@Api(tags = "CarController - Representa o controller de carros do usuário no sistema")
public class CarController {

    private final CarService service;

    /**
     * Recupera a lista de todos os carros do usuário logado
     *
     */
    @GetMapping()
    @ApiOperation(value = "Recupera a lista de todos os carros do usuário logado")
    public ResponseEntity<?> getAll(@RequestHeader("Authorization") String token) {
        try{
            return ResponseEntity.ok(this.service.findAll(token));
        }catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        }
    }

    /**
     * Salva um novo carro para usuário logado
     *
     * @param dto objeto com os dados do novo carro
     * @param token para verificação do usuário logado
     */
    @PostMapping()
    @ApiOperation(value = "Salva um novo carro para usuário logado")
    public ResponseEntity<?> save(@RequestHeader("Authorization") String token, @RequestBody CarDTO dto) {
        try{
            return ResponseEntity.ok(this.service.create(token, dto));
        }catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        }
    }

    /**
     * Recupera um carro do usuário logado por id
     *
     * @param id do carrro para perquisa
     * @param token para verificação do usuário logado
     */
    @GetMapping("{id}")
    @ApiOperation(value = "Recupera um carro do usuário logado por id")
    public ResponseEntity<?> getById(@RequestHeader("Authorization") String token, @PathVariable Long id){
        try{
            return ResponseEntity.ok(this.service.findById(token, id));
        }catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        }
    }

    /**
     * Exclui um carro do usuário logado por id
     *
     * @param id do carro a ser excluído
     * @param token para verificação do usuário logado
     */
    @DeleteMapping("{id}")
    @ApiOperation(value = "Exclui um carro do usuário logado por id")
    public ResponseEntity<?> deleteById(@RequestHeader("Authorization") String token, @PathVariable Long id) {
        try {
            this.service.delete(token, id);
            return ResponseEntity.ok(Boolean.TRUE);
        }catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        }
    }

    /**
     * Atualiza os dados de um carro do usuário logado
     *
     * @param dto objeto com os dados atualizado do carro
     * @param token para verificação do usuário logado
     */
    @PutMapping()
    @ApiOperation(value = "Atualiza os dados de um carro do usuário logado")
    public ResponseEntity<?> update(@RequestHeader("Authorization") String token, @RequestBody CarDTO dto) {
        try {
            return ResponseEntity.ok(this.service.update(token, dto));
        }catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        }
    }



}
