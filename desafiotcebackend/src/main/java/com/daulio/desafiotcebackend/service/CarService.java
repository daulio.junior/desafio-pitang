package com.daulio.desafiotcebackend.service;

import com.daulio.desafiotcebackend.dto.CarDTO;

import java.util.List;

/**
 * Representa interface do service com os métodos para carros no sistema.
 */
public interface CarService {

    List<CarDTO> findAll(String token);

    CarDTO findById(String token, Long id);

    CarDTO update(String token, CarDTO dto);

    CarDTO create(String token, CarDTO dto);

    void delete(String token, Long id);


}
