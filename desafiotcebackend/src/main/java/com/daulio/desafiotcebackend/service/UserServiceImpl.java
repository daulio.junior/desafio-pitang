package com.daulio.desafiotcebackend.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.daulio.desafiotcebackend.converter.UserConverter;
import com.daulio.desafiotcebackend.dto.UserDTO;
import com.daulio.desafiotcebackend.exception.RegraNegocioException;
import com.daulio.desafiotcebackend.model.Car;
import com.daulio.desafiotcebackend.model.User;
import com.daulio.desafiotcebackend.repository.UserRepository;
import com.daulio.desafiotcebackend.security.Consts;
import io.jsonwebtoken.ExpiredJwtException;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 * Representa service com os métodos para usuários no sistema.
 *
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService{

    private final UserRepository repository;
    private final UserConverter converter;

    /**
     * Recupera um usuário por id
     *
     * @param id do usuário para perquisa
     */
    @Override
    public UserDTO findById(Long id) {
        return this.converter.toDTO(Objects.requireNonNull(this.repository.findById(id).orElse(new User())));
    }

    /**
     * Atualiza os dados usuário
     *
     * @param dto objeto com os dados atualizado do usuário
     */
    @Override
    @Transactional
    public UserDTO update(UserDTO dto) throws RegraNegocioException {
        this.validateUpdateFields(dto);
        return this.save(dto);
    }

    /**
     * Salva um novo usuário
     *
     * @param dto objeto com os dados do novo usuário
     */
    @Override
    @Transactional
    public UserDTO create(UserDTO dto) throws RegraNegocioException {
        this.validateFields(dto);
        dto.setCreateAt(LocalDate.now());
        return this.save(dto);
    }

    /**
     * Salva um usuário
     *
     * @param dto objeto com os dados do novo usuário
     */
    public UserDTO save(UserDTO dto) throws RegraNegocioException {
        User userEntity = this.converter.toEntity(dto);
        if(Objects.nonNull(userEntity.getId()) && (Objects.isNull(userEntity.getPassword()))) {
                userEntity.setPassword(this.repository.findById(dto.getId())
                        .orElse(new User()).getPassword());

        }
        this.validateFieldsIsNull(userEntity);
        return this.converter.toDTO(this.repository.save(userEntity));
    }

    /**
     * Exclui um usuário por id
     *
     * @param id do usuário a ser excluído
     */
    @Override
    public void deleteById(Long id) {
        this.repository.deleteById(id);
    }

    /**
     * Recupera a lista de todos os usuários
     *
     */
    @Override
    public List<UserDTO> findAll() {
        return this.converter.toDTO(this.repository.findAll());
    }

    /**
     * Valida e existência de algum usuário com o e-mail ou login informado
     *
     * @param dto para verificação dos campos
     */
    private void validateFields(UserDTO dto) throws RegraNegocioException {
       if(this.repository.existsByEmail(dto.getEmail())) {
           throw new RegraNegocioException("Email already exists");
       }
       if(this.repository.existsByUsername(dto.getLogin())) {
           throw new RegraNegocioException("Login already exists");
       }
    }

    /**
     * Valida e existência de algum usuário com o e-mail ou login informado
     *
     * @param dto para verificação dos campos
     */
    private void validateUpdateFields(UserDTO dto) throws RegraNegocioException {
        if(this.repository.existsByEmailAndIdNot(dto.getEmail(),dto.getId())) {
            throw new RegraNegocioException("Email already exists");
        }
        if(this.repository.existsByUsernameAndIdNot(dto.getLogin(), dto.getId())) {
            throw new RegraNegocioException("Login already exists");
        }
    }

    /**
     * Retorna os dados do usuário a partir dos token JWT
     *
     * @param authorization para extração do token
     */
    @Override
    public UserDTO findAuthenticateUserByToken(String authorization) {

        String token = authorization.replace("Bearer ", Strings.EMPTY);
        if(token.isBlank()){
            throw new RegraNegocioException("Unauthorized");
        }

        try {
            DecodedJWT jwt = JWT.require(Algorithm.HMAC512(Consts.TOKEN_PASSWORD))
                    .build()
                    .verify(token);
            String username = jwt.getSubject();
            User user = this.repository.findByUsername(username).orElse(new User());
            return this.converter.toDTO(user);
        } catch (ExpiredJwtException e){
            throw new RegraNegocioException("Unauthorized - invalid session");
        } catch (Exception e) {
            throw new RegraNegocioException("Error: "+e.getMessage());
        }
    }

    /**
     * Remove um carro do usuário
     *
     * @param car - carro a ser removido
     */
    @Override
    @Transactional
    public void removeCar(Car car) {
        this.repository.findById(car.getUser().getId())
                .ifPresent(user -> {
                    user.getCars().remove(car);
                    this.repository.save(user);
                });
    }

    /**
     * Valida campos nulos
     *
     * @param dto para verificação dos campos
     */
    private void validateFieldsIsNull(User dto) throws RegraNegocioException {
        if(Objects.isNull(dto.getBirthday())||dto.getFirstName().isBlank()
            ||dto.getLastName().isBlank()||dto.getEmail().isBlank()
            ||dto.getUsername().isBlank()||dto.getPassword().isBlank()
            ||dto.getPhone().isBlank()){
            throw new RegraNegocioException("Missing fields");
        }
    }
}
