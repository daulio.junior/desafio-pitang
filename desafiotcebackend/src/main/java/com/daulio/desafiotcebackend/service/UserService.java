package com.daulio.desafiotcebackend.service;


import com.daulio.desafiotcebackend.dto.UserDTO;
import com.daulio.desafiotcebackend.model.Car;

import java.util.List;

/**
 * Representa interface do service com os métodos para usuários no sistema.
 */
public interface UserService {

    UserDTO findById(Long id);

    UserDTO update(UserDTO dto);

    UserDTO create(UserDTO dto);

    void deleteById(Long id);

    List<UserDTO> findAll();

    UserDTO findAuthenticateUserByToken(String authorization);

    void removeCar(Car car);

}
