package com.daulio.desafiotcebackend.service;

import com.daulio.desafiotcebackend.converter.CarConverter;
import com.daulio.desafiotcebackend.converter.UserConverter;
import com.daulio.desafiotcebackend.dto.CarDTO;
import com.daulio.desafiotcebackend.dto.UserDTO;
import com.daulio.desafiotcebackend.exception.RegraNegocioException;
import com.daulio.desafiotcebackend.model.Car;
import com.daulio.desafiotcebackend.model.User;
import com.daulio.desafiotcebackend.repository.CarRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

/**
 * Representa service com os métodos para carros no sistema.
 *
 */
@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {

    private final CarRepository repository;
    private final CarConverter converter;
    private final UserService userService;
    private final UserConverter userConverter;

    /**
     * Recupera a lista de todos os carros do usuário logado
     *
     */
    @Override
    public List<CarDTO> findAll(String token) throws RegraNegocioException {
        UserDTO user = this.userService.findAuthenticateUserByToken(token);
        return this.converter.toDTO(this.repository.findAllByUserIdOrderByQtdUsoDesc(user.getId()));
    }

    /**
     * Recupera um carro do usuário logado por id
     *
     * @param id do carrro para perquisa
     * @param token para verificação do usuário logado
     */
    @Override
    public CarDTO findById(String token, Long id) throws RegraNegocioException {
        UserDTO user = this.userService.findAuthenticateUserByToken(token);
        Car car = this.repository.findByIdAndUserId(id, user.getId())
                .orElseThrow(() -> new RegraNegocioException("Entity not found: " + id));

        // Increment qtdUso by 1
        car.setQtdUso(car.getQtdUso()+ 1);
        this.repository.save(car);

        return this.converter.toDTO(car);
    }

    /**
     * Atualiza os dados de um carro do usuário logado
     *
     * @param dto objeto com os dados atualizado do carro
     * @param token para verificação do usuário logado
     */
    @Override
    @Transactional
    public CarDTO update(String token, CarDTO dto) throws RegraNegocioException {
        if(this.repository.existsByLicensePlateAndIdNot(dto.getLicensePlate(), dto.getId())){
            throw  new RegraNegocioException("License plate already exists");
        }
        return this.save(token, dto);
    }

    /**
     * Salva um novo carro para usuário logado
     *
     * @param dto objeto com os dados do novo carro
     * @param token para verificação do usuário logado
     */
    @Override
    @Transactional
    public CarDTO create(String token, CarDTO dto) throws RegraNegocioException {
        if(this.repository.existsByLicensePlate(dto.getLicensePlate())){
            throw  new RegraNegocioException("License plate already exists");
        }
        return this.save(token, dto);
    }

    /**
     * Salva um carro para usuário logado
     *
     * @param dto objeto com os dados do novo carro
     * @param token para verificação do usuário logado
     */
    public CarDTO save(String token, CarDTO dto) throws RegraNegocioException {
        validationFields(dto);
        Car car = this.converter.toEntity(dto);
        User user = this.userConverter.toEntity(this.userService.findAuthenticateUserByToken(token));
        car.setUser(user);
        return this.converter.toDTO(this.repository.save(car));
    }

    /**
     * Exclui um carro do usuário logado por id
     *
     * @param id do carro a ser excluído
     * @param token para verificação do usuário logado
     */
    @Override
    @Transactional
    public void delete(String token, Long id) throws RegraNegocioException {
        UserDTO userDTO = this.userService.findAuthenticateUserByToken(token);
        this.repository.findByIdAndUserId(id, userDTO.getId())
                .ifPresent(car->{
                    this.userService.removeCar(car);
                    this.repository.delete(car);
                });
    }

    /**
     * Valida campos nulos
     *
     * @param dto para verificação dos campos
     */
    private void validationFields(CarDTO dto) throws RegraNegocioException{
        if(Objects.isNull(dto.getYear())||dto.getColor().isBlank()
                ||dto.getLicensePlate().isBlank()||dto.getModel().isBlank()){
            throw new RegraNegocioException("Missing fields");
        }
    }
}
