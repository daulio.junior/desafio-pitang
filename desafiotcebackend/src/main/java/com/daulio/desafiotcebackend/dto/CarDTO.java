package com.daulio.desafiotcebackend.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;

/**
 * Classe DTO para requisição e resposta dos endpoints de carros.
 *
 */
@Getter @Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CarDTO implements Serializable {


    private static final long serialVersionUID = -3430494073174194399L;
    private Long id;

    private Integer year;

    private String licensePlate;

    private String model;

    private String color;

    private Integer qtdUse;


}
