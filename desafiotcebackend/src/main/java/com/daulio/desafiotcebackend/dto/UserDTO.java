package com.daulio.desafiotcebackend.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * Classe DTO para requisição e resposta dos endpoints de usuários.
 *
 */
@Getter @Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDTO implements Serializable {


    private static final long serialVersionUID = -7301012086618768491L;
    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private LocalDate birthday;

    private String login;

    private String password;

    private String phone;

    private LocalDate createAt;

    private LocalDate lastLogin;

    private List<CarDTO> cars;


}
