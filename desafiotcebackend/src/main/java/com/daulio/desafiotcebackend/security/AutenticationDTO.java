package com.daulio.desafiotcebackend.security;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

/**
 * Classe DTO para resposta do login no sistema
 *
 */
@Getter
@Setter
@RequiredArgsConstructor
public class AutenticationDTO {
    private long id;
    private String firstName;
    private String lastName;
    private String email;
    private LocalDate birthday;
    private String phone;
    private String login;
    private String token;
    private String createAt;
}
