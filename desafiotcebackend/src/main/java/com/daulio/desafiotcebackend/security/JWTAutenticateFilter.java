package com.daulio.desafiotcebackend.security;

import com.auth0.jwt.algorithms.Algorithm;
import com.daulio.desafiotcebackend.exception.RegraNegocioException;
import com.daulio.desafiotcebackend.model.User;
import com.daulio.desafiotcebackend.model.UserDetailsModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Classe para configurações e respostas de autenticação
 *
 */
public class JWTAutenticateFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;

    public JWTAutenticateFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
        setFilterProcessesUrl("/api/signin");
    }


    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws AuthenticationException {
        try {
            User user = new ObjectMapper()
                    .readValue(request.getInputStream(), User.class);

            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    user.getUsername(),
                    user.getPassword(),
                    new ArrayList<>()
            ));

        } catch (IOException e) {
            throw new RegraNegocioException("Invalid login or password");
        }

    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {

        UserDetailsModel userData = (UserDetailsModel) authResult.getPrincipal();

        String token = com.auth0.jwt.JWT.create().
                withSubject(userData.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + Consts.TOKEN_EXPIRATION))
                .sign(Algorithm.HMAC512(Consts.TOKEN_PASSWORD));


        AutenticationDTO autenticationDTO = new AutenticationDTO();
        autenticationDTO.setToken(token);

        userData.getUser().ifPresent(user -> {
            autenticationDTO.setId(user.getId());
            autenticationDTO.setFirstName(user.getFirstName());
            autenticationDTO.setLastName(user.getLastName());
            autenticationDTO.setBirthday(user.getBirthday());
            autenticationDTO.setEmail(user.getEmail());
            autenticationDTO.setPhone(user.getPhone());
            autenticationDTO.setLogin(userData.getUsername());
            autenticationDTO.setCreateAt(user.getCreateAt().toString());
        });


        String json = new Gson().toJson(autenticationDTO);

        response.getWriter().write(json);
        response.getWriter().flush();


    }
}