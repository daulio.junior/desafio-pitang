package com.daulio.desafiotcebackend.security;


import com.daulio.desafiotcebackend.model.User;
import com.daulio.desafiotcebackend.model.UserDetailsModel;
import com.daulio.desafiotcebackend.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Classe para validação de login do usuário
 *
 */
@Component
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = this.userRepository.findByUsername(username);
        user.ifPresentOrElse(
                user1 -> {
                    user1.setLastLogin(LocalDate.now());
                    this.userRepository.save(user1);
                },
                () -> {
                    throw new UsernameNotFoundException("Invalid login or password");
                }
        );

        return new UserDetailsModel(user);
    }
}
