package com.daulio.desafiotcebackend.repository;


import com.daulio.desafiotcebackend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Repository para entidade User
 *
 */
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String login);

    boolean existsByEmail(String email);

    boolean existsByEmailAndIdNot(String email, Long id);

    boolean existsByUsername(String login);

    boolean existsByUsernameAndIdNot(String login, Long id);

}
