package com.daulio.desafiotcebackend.repository;

import com.daulio.desafiotcebackend.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Repository para entidade Car
 *
 */
public interface CarRepository extends JpaRepository<Car, Long> {

    Optional<Car> findByIdAndUserId(Long id, Long userId);

    List<Car> findAllByUserIdOrderByQtdUsoDesc(Long userId);

    boolean existsByLicensePlate(String licensePlate);

    boolean existsByLicensePlateAndIdNot(String licensePlate, Long id);

}
