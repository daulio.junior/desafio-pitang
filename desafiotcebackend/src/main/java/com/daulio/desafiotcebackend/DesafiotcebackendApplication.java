package com.daulio.desafiotcebackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
public class DesafiotcebackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafiotcebackendApplication.class, args);
	}

}
