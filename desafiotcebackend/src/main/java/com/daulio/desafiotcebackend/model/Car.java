package com.daulio.desafiotcebackend.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Representa entidade do banco de dados para carros.
 *
 */
@Entity
@Table(name="CARS")
@Getter
@Setter
public class Car implements Serializable {
    private static final long serialVersionUID = -7827668159276854748L;

    @Id
    @Column(name = "id_car")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "year_model")
    private Integer year;

    private String licensePlate;

    private String model;

    private String color;

    @Column(name = "qtd_uso", columnDefinition = "integer default 0")
    private Integer qtdUso = 0;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

}
