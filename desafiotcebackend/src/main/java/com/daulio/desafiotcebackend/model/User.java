package com.daulio.desafiotcebackend.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * Representa entidade do banco de dados para usuários.
 *
 */
@Entity
@Table(name="USERS")
@Getter @Setter
public class User implements Serializable {
    private static final long serialVersionUID = -992660423645065538L;
    @Id
    @Column(name = "id_user")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private LocalDate birthday;

    @Column(name = "login")
    private String username;

    private String password;

    private String phone;

    private LocalDate createAt;

    private LocalDate lastLogin;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<Car> cars;


}
