package com.daulio.desafiotcebackend.exception;

/**
 * Classe para tratamento de erro
 *
 */
public class RegraNegocioException extends RuntimeException {

    public RegraNegocioException(String message) {
        super(message);
    }

}
