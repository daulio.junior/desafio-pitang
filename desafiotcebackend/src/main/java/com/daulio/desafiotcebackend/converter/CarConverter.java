package com.daulio.desafiotcebackend.converter;


import com.daulio.desafiotcebackend.dto.CarDTO;
import com.daulio.desafiotcebackend.model.Car;
import com.daulio.desafiotcebackend.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Classe para converção de DTO para Entity e de Entity para DTO dos dados de carros
 *
 */
@Component
@RequiredArgsConstructor
public class CarConverter {

    private final UserRepository userRepository;

    public Car toEntity(CarDTO dto) {
        Car e = new Car();
        e.setId(dto.getId());
        e.setYear(dto.getYear());
        e.setColor(dto.getColor());
        e.setModel(dto.getModel());
        e.setLicensePlate(dto.getLicensePlate());
        e.setQtdUso(Objects.isNull(dto.getQtdUse())?0: dto.getQtdUse());
        return  e;
    }

    public CarDTO toDTO(Car car) {
        return CarDTO.builder()
                .id(car.getId())
                .color(car.getColor())
                .year(car.getYear())
                .licensePlate(car.getLicensePlate())
                .model(car.getModel())
                .qtdUse(car.getQtdUso())
                .build();
    }

    public List<CarDTO> toDTO(List<Car> list) {
        return list.stream().map(this::toDTO).collect(Collectors.toList());
    }

    public List<Car> toEntity(List<CarDTO> list) {
        return list.stream().map(this::toEntity).collect(Collectors.toList());
    }
}
